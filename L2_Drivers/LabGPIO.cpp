#include "LabGPIO.hpp"

LabGPIO::LabGPIO(uint8_t pin, uint8_t port) : pin_num(pin), port_num(port)
{
	if(port == 0)
		GPIO_link = (LPC_GPIO_TypeDef*) (LPC_GPIO0_BASE);
	else if(port == 1)
		GPIO_link = (LPC_GPIO_TypeDef*) (LPC_GPIO1_BASE);
		
}
	
void LabGPIO::setAsInput()
{
	GPIO_link->FIODIR &= ~(1 << pin_num);
}    

void LabGPIO::setAsOutput()
{
	GPIO_link->FIODIR |= (1 << pin_num);
}

void LabGPIO::setDirection(bool output)
{
	if(output == true)
		setAsOutput();
	else
		setAsInput();	
}

void LabGPIO::setHigh()
{
	GPIO_link->FIOSET = (1 << pin_num);
}

void LabGPIO::setLow()
{
	GPIO_link->FIOCLR = (1 << pin_num);
}

void LabGPIO::set(bool high)
{
	if(high == true)
		setHigh();
	else
		setLow();
}

bool LabGPIO::getLevel()
{
	if (GPIO_link->FIOPIN & (1 << pin_num))
	{
		return true; // Switch is logical HIGH
	}
	else 
	{
		return false; // Switch is logical LOW
	}
}

LabGPIO::~LabGPIO()
{
	setAsInput();	
}

void LabGPIO::toggle()
{
	if (GPIO_link->FIOPIN & (1 << pin_num))
	{
        GPIO_link->FIOCLR = (1 << pin_num);
	}
	else
	{
		GPIO_link->FIOSET = (1 << pin_num);
	}				
}
