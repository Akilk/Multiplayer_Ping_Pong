#include "LabSPI.hpp"

constexpr LPC_SSP_TypeDef * LabSPI::SSP[];
SemaphoreHandle_t LabSPI::SPI_Mutex;

bool LabSPI::init(Peripheral peripheral, uint8_t data_size_select, FrameModes format, uint8_t divide)
{
	// input validation
	if(peripheral > SSP1 || (divide < 2 || (divide % 2) || divide > 254) || (data_size_select < 4 || data_size_select > 16) || (format > Frame_Microwire))
		return 0;
	
	if(peripheral == SSP0)
	{
		LPC_SC->PCONP |= (1<<21);
		LPC_SC->PCLKSEL1 &= ~(3 << 10); // Clear clock Bits
		LPC_SC->PCLKSEL1 |=  (1 << 10); // CLK / 1


        //initialize pins
		LPC_PINCON->PINSEL0 &= ~(3 << 30); // Clear bits SCK0
        LPC_PINCON->PINSEL1 &= ~((3 << 2) | (3 << 4)); // Clear bits MISO0, MOSI0
        LPC_PINCON->PINSEL0 |= (2 << 30); // Set SCK0
        LPC_PINCON->PINSEL0 |= ((2 << 2) | (2 << 4)); // Set MISO0, MOSI0
	}
	else if(peripheral == SSP1)
	{
		LPC_SC->PCONP |= (1<<10);
		LPC_SC->PCLKSEL0 &= ~(3 << 20); // Clear clock Bits
		LPC_SC->PCLKSEL0 |=  (1 << 20); // CLK / 1
	
		//initialize pins
		// P0.7 - SCK1
		// P0.8 - MISO1
		// P0.9 - MOSI1
		
		// Select MISO, MOSI, and SCK pin-select functionality
		
		LPC_PINCON->PINSEL0 &= ~( (3 << 14) | (3 << 16) | (3 << 18) );
		LPC_PINCON->PINSEL0 |=  ( (2 << 14) | (2 << 16) | (2 << 18) );
	}
	periph = peripheral; // Store peripheral number
	SSP[peripheral]->CPSR = divide;
	SSP[peripheral]->CR0 |= (data_size_select - 1);
	SSP[peripheral]->CR1 |= (1<<1);	// SSP enable;
	SSP[peripheral]->CR0 &= ~(3 << 4);
	SSP[peripheral]->CR0 |= (format << 4);
	
	SPI_Mutex = xSemaphoreCreateMutex();
	
	return 1;
}

uint8_t LabSPI::transfer(uint8_t send)
{
    SSP[periph]->DR = send;
    while(SSP[periph]->SR & (1 << 4)); // Wait until SSP is busy
    return SSP[periph]->DR;
}
