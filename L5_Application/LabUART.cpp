#include <LabUART.h>

QueueHandle_t LabUART::q;
constexpr LPC_UART_TypeDef * LabUART::UART[];
uart_periph LabUART::periph;

bool LabUART::init_UART(uart_periph peripheral, uint32_t baudrate)
{
    q = xQueueCreate(10, sizeof(uint8_t));
    uint32_t baud_value;

    periph = peripheral;

    if(periph == UART2)
    {
        // Powering up UART 2
        LPC_SC->PCONP &= ~(1 << 24);
        LPC_SC->PCONP |= (1 << 24);

        //Setting Clock
        LPC_SC->PCLKSEL1 &= ~(1 << 16);
        LPC_SC->PCLKSEL1 |= (1 << 16);

        //Setting DLAB bit for UART2 as 1
        LPC_UART2->LCR |= (1 << 7);

        baud_value = ( sys_get_cpu_clock() / (16 * baudrate )) + 0.5;
        LPC_UART2->DLL =  baud_value & 0xFF;
        LPC_UART2->DLM = (baud_value >> 0x08) & 0xFF;

        //Setting DLAB bit for UART2 as 0
        LPC_UART2->LCR &= ~(1 << 7);

        //Enabling TXD2 pin for transmit operation
        LPC_PINCON->PINSEL4 &= ~(3 << 16);
        LPC_PINCON->PINSEL4 |= (2 << 16);

        //Enabling RXD2 pin for Receive operation
        LPC_PINCON->PINSEL4 &= ~(3 << 18);
        LPC_PINCON->PINSEL4 |= (2 << 18);

        //Frame size in bits
        LPC_UART2->LCR &= ~(3);
        LPC_UART2->LCR |= (3);

        //2 Stop bits
        LPC_UART2->LCR &= ~(1 << 2);
        LPC_UART2->LCR |= (1 << 2);

        NVIC_EnableIRQ(UART2_IRQn);

        LPC_UART2->IER |= 1 |(1 << 2);

        isr_register(UART2_IRQn, my_uart_rx_intr);
    }
    else if(periph == UART3)
    {
        // Powering up UART 3
        LPC_SC->PCONP |= (1 << 25);

        //Setting Clock
        LPC_SC->PCLKSEL1 |= (1 << 18);

        //Setting DLAB bit for UART2 as 1
        LPC_UART3->LCR |= (1 << 7);

        baud_value = ( sys_get_cpu_clock() / (16 * baudrate )) + 0.5;
        LPC_UART3->DLL =  baud_value & 0xFF;
        LPC_UART3->DLM = (baud_value >> 0x08) & 0xFF;

        //Setting DLAB bit for UART2 as 0
        LPC_UART3->LCR &= ~(1 << 7);

        //Enabling TXD3 and RXD3 pin

        LPC_PINCON->PINSEL9 &= ~(0xF << 24); // Clear values
        LPC_PINCON->PINSEL9 |=  (0xF << 24); // Set values for UART3 Rx/Tx

        //Frame size in bits
        LPC_UART3->LCR &= ~(3);
        LPC_UART3->LCR |= (3);

        //2 Stop bits
        LPC_UART3->LCR &= ~(1 << 2);
        LPC_UART3->LCR |= (1 << 2);

        NVIC_EnableIRQ(UART3_IRQn);

        LPC_UART3->IER |= 1 |(1 << 2);

        isr_register(UART3_IRQn, my_uart_rx_intr);
    }
    else
        return false;

    //Setting DLAB bit for UART2 as 1
    UART[periph]->LCR |= (1 << 7);

    baud_value = ( 96000000UL / (16 * baudrate )) + 0.5;
    UART[periph]->DLL =  baud_value & 0xFF;
    UART[periph]->DLM = (baud_value >> 0x08) & 0xFF;

    //Setting DLAB bit for UART2 as 0
    UART[periph]->LCR &= ~(1 << 7);

    //Frame size in bits
    UART[periph]->LCR &= ~(3);
    UART[periph]->LCR |= (3);

    //2 Stop bits
    UART[periph]->LCR &= ~(1 << 2);
    UART[periph]->LCR |= (1 << 2);

    UART[periph]->IER |= 1 |(1 << 2);

    isr_register((periph == UART2)? UART2_IRQn : UART3_IRQn, LabUART::my_uart_rx_intr);

    return true;
}

void LabUART::my_uart_rx_intr(void)
{
    while(! (UART[periph]->LSR & 1));
    char data = UART[periph]->RBR;

    xQueueSend(LabUART::q, &data, portMAX_DELAY);

    u0_dbg_printf("\n Interrupt Function received character");
}

bool LabUART::receive(char *ch)
{
    if (xQueueReceive(LabUART::q, ch, 0)) {
        u0_dbg_printf("\n Got %d char from my UART", *ch);
        return true;
    }

    return false;
}

void LabUART::transmit( char x){
    u0_dbg_printf("\n Sending %d \n",x);
    while(! (UART[periph]->LSR & (1 << 6)));
    UART[periph]->THR = x;
}

void LabUART::printBytes(uint8_t* buf, uint32_t len)
{
    // transfer all bytes to HW Tx FIFO
    while ( len != 0 )
    {
        // send next byte
        LabUART::transmit(*buf);

        // update the buf ptr and length
        buf++;
        len--;
    }
}

void LabUART::printString(char* buf)
{
    while ( *buf != '\0' )
    {
        // send next byte
        LabUART::transmit((uint8_t)*buf);

        // update the buf ptr
        buf++;
    }
}
