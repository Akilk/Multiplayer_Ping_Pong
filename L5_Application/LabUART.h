#ifndef LABUART_H
#define LABUART_H

#include <examples/rn_xv_task.hpp>
#include <FreeRTOS.h>
#include <FreeRTOSConfig.h>
#include <lpc_isr.h>
#include <LPC17xx.h>
#include <portmacro.h>
#include <printf_lib.h>
#include <queue.h>
#include <singleton_template.hpp>
#include <sys/_stdint.h>
#include <task.h>
#include <uart2.hpp>
#include <cstdio>
#include <core_cm3.h>
#include <lpc_isr.h>

#include <LPC17xx.h>
#include <sys/_stdint.h>

enum uart_periph
{
    UART2 = 0,
    UART3 = 1
};

class LabUART
{
private:
      static QueueHandle_t q ;  // Queue for storing characters
      static constexpr LPC_UART_TypeDef * UART[2] = {LPC_UART2, LPC_UART3};
      static uart_periph periph;    // static variable for storing the peripheral
public:

    /**
    * 1) Powers on corresponding Uart peripheral
    * 2) Set peripheral clock
    * 3) Set Baudrate
    * 4) Sets pins for specified peripheral to RXD, TXD
    * 5) Enable receive interrupt
    *
    * @param periph which peripheral UART0 or UART1 you want to select.
    * @param baudrate is the communication frequency at which you want to communicate
    *
    * @return true if initialization was successful
    */

    bool init_UART(uart_periph periph, uint32_t baudrate);

    /**
     * Transfers a byte via UART to an external device using the THR data register.
     *
     * @param x byte to transmit via UART.
     */

    void transmit(char x);

    /**
     * Function callback that will be called when a configured UART interrupt is generated.
     */

    static void my_uart_rx_intr(void);

    /**
     * Receives a byte via UART from an external device using the RBR data register.
     *
     * @param ch Output byte received on configured UART
     *
     * @return true if a byte was received and found in the queue
     */

    bool receive(char* ch);

    /**
     * transmits the specified number of bytes via configured UART
     *
     * @param buf input buffer to be transferred
     * @param len length of bytes to be transferred
     *
     */

    void printBytes(uint8_t* buf, uint32_t len);

    /**
     * Transmits the character array over configured UART channel
     *
     * @param buf string of characters to be sent
     *
     */

    void printString(char* buf);
};

#endif
