#include "tasks.hpp"
#include "examples/examples.hpp"
#include<FreeRTOS.h>
#include<queue.h>
#include<event_groups.h>
#include<string>
#include<sstream>
#include<storage.hpp>
#include<io.hpp>
#include <printf_lib.h>
#include<stdio.h>
#include<time.h>
#include "L5_Application/RGBLed_Matrix.hpp"
#include<math.h>
#include "utilities.h"
#include "io.hpp"
#include "wireless/wireless.h"

RGBmatrixPanel matrix;
uint8_t x1_l = 31, y1_l = 15, x2_l = 31, y2_l = 13;

#define DEBUG 0

typedef enum
{
    top_left,
    top_right,
    down_left,
    down_right
}movement_towards;


static uint8_t ball_x1 = 5, ball_y1 = 5;

/*    top-left          top-right
 *                ball
 *    down-left         down-right
 */

void ball_task(void *p)
{
    static movement_towards ball_movement = down_right;

    while(1)
    {
        if(ball_y1 == 0)  // ball hit the bottom, need to reflect back according to direction of ball
        {
            if(ball_movement == down_right)
            {
                if(ball_x1 == 0)        // corner ( 0, 0)
                {
                    ball_movement = top_left;
                    ball_x1 = 1;
                    ball_y1 = 1;
                }
                else
                {
                    ball_movement = top_right;
                    ball_y1 = 1;
                    ball_x1--;
                }
            }
            else if(ball_movement == down_left)
            {
                if(ball_x1 == 31)       // corner (31, 0)
                {
                    ball_movement = top_right;
                    ball_x1 = 30;
                    ball_y1 = 1;
                }
                else
                {
                    ball_movement = top_left;
                    ball_y1 = 1;
                    ball_x1++;
                }
            }
        }
        else if(ball_x1 == 0)
        {
            if(ball_movement == top_right)
            {
                if(ball_y1 == 15)    // top right corner (0,15)
                {
                    ball_movement = down_left;
                    ball_x1 = 1;
                    ball_y1 = 14;
                }
                else
                {
                    ball_movement = top_left;
                    ball_x1 = 1;
                    ball_y1++;
                }
            }
            else if(ball_movement == down_right)
            {
                ball_movement = down_left;
                ball_x1 = 1;
                ball_y1--;
            }
        }
        else if(ball_y1 == 15)
        {
            if(ball_movement == top_left)
            {
                if(ball_x1 == 31)   // top left corner (31,15)
                {
                    ball_movement = down_right;
                    ball_x1 = 30;
                    ball_y1--;
                }
                else
                {
                    ball_movement = down_left;
                    ball_x1++;
                    ball_y1 = 14;
                }
            }
            else if(ball_movement == top_right)
            {
                ball_movement = down_right;
                ball_x1--;
                ball_y1 = 14;
            }
        }
        else if(ball_x1 == 31)
        {
            if(ball_movement == top_left)
            {
                ball_movement = top_right;
                ball_x1 = 30;
                ball_y1++;
            }
            else if(ball_movement == down_left)
            {
                ball_movement = down_right;
                ball_x1 = 30;
                ball_y1--;
            }
        }
        else
        {
            if(ball_movement == top_right)
            {
                ball_x1--;
                ball_y1++;
            }
            else if(ball_movement == top_left)
            {
                ball_x1++;
                ball_y1++;
            }
            else if(ball_movement == down_right)
            {
                ball_x1--;
                ball_y1--;
            }
            else if(ball_movement == down_left)
            {
                ball_x1++;
                ball_y1--;
            }
        }

#if DEBUG
        printf("Ball x: %d y : %d \n", ball_x1, ball_y1);
#endif

        matrix.fill(matrix.Color333(0, 0, 0));      // Clear screen
        matrix.drawPixel(ball_x1, ball_y1, matrix.Color333(8, 0, 0));
        vTaskDelay(100);
    }
}

void ledTask(void* p)
{
    static bool done = false;

    while(1)
    {
        if(!done)
        {
            if(SW.getSwitch(1))
            {
                if(y1_l == 15)
                {
                    y1_l = 3;
                    y2_l = 0;
                }
                else
                {
                    y1_l++;
                    y2_l++;
                }

                matrix.fill(matrix.Color333(0, 0, 0));
            }

            if(SW.getSwitch(2))
            {
                if(y2_l == 0)
                {
                    y1_l = 15;
                    y2_l = 13;
                }
                else
                {
                    y1_l--;
                    y2_l--;
                }
                matrix.fill(matrix.Color333(0, 0, 0));
            }

            printf(" Y1 : %d Y2 : %d\n", y1_l, y2_l);
            matrix.drawLine(x1_l, y1_l, x2_l, y2_l, matrix.Color333(8, 0, 0));
            vTaskDelay(500);
        }
    }
}

int main(void)
{
    const uint16_t STACK_SIZE = 2048;
    scheduler_add_task(new terminalTask(PRIORITY_LOW));
    xTaskCreate(ball_task, "ball_task", STACK_SIZE * 2, NULL, 1, NULL);

    const bool run_1Khz = true;
    scheduler_add_task(new periodicSchedulerTask(run_1Khz));

    scheduler_start();
    return -1;
}
